# Language and tool support

## Protobuf
Protobuf information types are defined in the protobuf language. Information types are thereafter used to generate programming language specific representations that are used in programs.
For a developer this is very convenient. 

In a Java-environment, the build system (e.g. gradle) can be configured so that this is done automatically as part of the build process.

In a .Net-environment, the build system (e.g. MSBuild) can similarly be configured to run the protobuf compiler to generate C#-files for the protobuf information types.
Microsoft Visual Studio has built in support (a nuget package) for protobuf.

More about C# support for protobuf: 

* ["Only proto3 messages are supported by the C# code generator."](https://developers.google.com/protocol-buffers/docs/reference/csharp-generated)
* [History of C# protobufs](https://github.com/google/protobuf/tree/master/csharp#history-of-c-protobufs)

## JSON
In Java programs developers would use Jackson (known as "JSON for Java") to process JSON-messages. Jackson and other libraries, such as Lombok, makes it quite convenient to work with JSON in Java. A separate Java class, however, has to be implemented for each JSON message type.

One advantage for JSON is that it is human readable and easier to consume from MQTT with standard tools, such as _mosquitto_sub_. Debugging on the consumer side is therefore easier with JSON than with protobuf.

## Distribution of message format versions
Protobuf information types should be made available as source code, for example in a Git-repository.
JSON would be defined with examples and possibly with JSON schemas, this could also be done in a Git-repository.

## Findings
A protobuf information type needs to be defined only once, and by means of the protobuf compiler, support for a number of programming languages is supported.

JSON is very well supported by all common programming languages, but is handled differently in different languages.

As such, a finding is that protobuf is very programmer friendly, while using JSON requires more work for the developer.
