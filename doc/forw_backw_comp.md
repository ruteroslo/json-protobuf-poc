# Forward and backward compatibility
With forward compatibility we mean that if changes are made to the message format by the producer, clients that receieve the messages should still work.
With backward compatibility we mean that clients that are made to consume a certain message format should be able to read earlier versions of the message.

hvor passer dette:
## proto2 and proto3
There are two syntaxes for Protocol buffers: proto2 and proto3.
These are the main differences:

* In proto2, fields can be required or optional, in proto3 all fields are optional.
* In proto2, fields can have default values, in proto3 non-zero default values are not supported.
* Proto2 has support for C++, Java, C#, and Python. Third-party implementation is available for JavaScript. (Note the new C# implementation only supports proto3)
* Proto3 has support for C++, Jaca, C#, Python, Go, Ruby, Objective-C and JavaScript (Beta). Third-party implementations are available for Perl, PHP, R, Scala, Swift and Julia. (source: https://en.wikipedia.org/wiki/Protocol_Buffers)
* Proto2 had groups of fields, in proto3 nested messages are used instead.
* Proto3 has some new types such as Timestamp, Duration, Struct, ListValue and others (see )
* Proto3 is more compatible with JSON. Proto2 has some features that will be ignored when converting a message to JSON (e.g. extensions and unknown fields)

Some online sources (e.g. https://grpc.io/docs/guides/) recommend using proto3 unless you have a good reason to use proto2. For use in ITxPT the question of proto2 versus proto3 needs to be further investigated.

## Projects to test compatibility
We use two java projects, _src/produce_ and _src/consume_, to test forward/backward compatibility.
We start out with identical protobuf and JSON formats in the two and can run the main programs in _src/produce_ to produce messages that can be consumed by the main program in the _src/consume_ project.

## Forward compatibility
To test forward compatibility we make changes to the consumer and checks if we can still consume messages in the 'old' format produced by the producer.

### Rename a field in the consumer
Rename fields in the _consumer_-project and then see if you can still consume the messages produced.

#### Protobuf
Protobuf: rename fields "passengers_in" and "passengers_out" to "in" and "out". Rebuild the project.
-> this works with no issues since the fields are in the same position in the byte stream.

#### JSON
JSON: rename the fields in the APCEvent.java class, and in the JSON schema for the APC message. Rebuild the project.
-> the consumer now fails because it uses the names of the fields when deserialising the MQTT message payload. And the names of the incoming messages does not match with the expected names.

### Add a field in the consumer
Add a field to the messages in the _consumer_-project and then see if you can still consume the messages produced.

#### Protobuf
Add the field "arrival_time" to the apc.proto file, rebuild the project and then try to consume messages again.
-> This works fine, since the new field has an offset which is not used already. In the consumer, the new field gets the value "0".

#### JSON
Add the same field to both the ApcEvent.java class and the APC JSON schema.
-> This works fine.

### Remove a field
Remove a field and then see if you can still consume the messages produced.

#### Protobuf
Remove the "passengers_out" field in apc.proto, and rebuild the project.
-> this works fine, the passengers_out field in the produced message is ignored on the consumer side.

#### JSON
Remove the field "passengersOut" from the APCEvent.java class.
-> the consumer fails with the error. "SEVERE: Unrecognized field "passengersOut" (cl...", but can fix this by adding annotation:  @JsonIgnoreProperties(ignoreUnknown = true).

### Change a value in an enum in the consumer

#### Protobuf
Change the CountQuality enum value "ABSENT" to "UNKNOWN".
-> this works fine. Messages that are produced with CountQuality "ABSENT" are still consumed. In the consumer "ABSENT" is now "UNKNOWN".

#### JSON
-> With the same change in JSON, the consumer is now **not** able to consume messages with CountQuality "ABSENT".

## Backward compatibility
To test backward compatibility we make changes on the producer side.

### Rename a field

#### Protobuf
In the producer, change the "passengers_out" field to "out", run "gradle build", and then produce proto-messages.
-> The messages can still be consumed in the consumer.

#### JSON
Make the same change when producing a JSON-message.
-> The consumer now fails with "UnrecognizedPropertyException: Unrecognized field "out""

### Add a field to the produced message

#### Protobuf
Add a field "arrivalTime" to the apc.proto file on the producer side, run "gradle build", and produce a message.
-> This works fine, the consumer is still able to consume

#### JSON
Do the same change to the JSON-message in the producer.
-> the consumer now gets an error: "SEVERE: Unrecognized field "arrivalTime"", but can fix this by adding annotation:  @JsonIgnoreProperties(ignoreUnknown = true)

### Remove a field
This is the same as adding a field on the consumer side and will work fine in both protobuf and JSON.

### Add a value in an enum in the producer

#### Protobuf
Add an enum value to CountQuality in apc.proto.
-> the consumer can still consume the messages. Messages that has the new enum value will have CountQuality = 4 (the new value's offset in the enum),

#### JSON
With the same change to the JSON message, the consumer fails. This can, however, be fixed by writing a custom deserializer for the enum field (see for example here: https://stackoverflow.com/questions/16446619/how-to-ignore-enum-fields-in-jackson-json-to-object-mapping)

## Findings
Protobuf seems to be by design much more developer-friendly when it comes to forward/backwards compatibility. JSON-support can be implemented in a way that provides forward/backwards compatibility, too, but this requires more custom development and will have to be done differently in different program languages.
