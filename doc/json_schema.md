# JSON Schema support

## Abstract
The purpose of this section is to shed some light on the support, pros and cons for applying JSON Schema support to ensure interoperability between systems complying with the ITxPT standard.

To illustrate the use of JSON Schema in ITxPT we have composed schemas for APC and GPS message formats:

* [JSON Schema files for APC and GPS message](../src/schemas).

## JSON Schema (Core, Validation)

* describes your existing data format
* clear, human- and machine-readable documentation
* complete structural validation, useful for automated testing
* validating client-submitted data

JSON Schema documents are identified by URIs, which can be used in HTTP Link headers, and inside JSON Schema documents to allow recursive definitions.

The JSON document being validated or described we call the instance, and the document containing the description is called the schema.

JSON Schema describes the structure of a JSON document (for instance, required properties and length limitations). Applications can use this information to validate instances (check that constraints are met), or inform interfaces to collect user input such that the constraints are satisfied.

Source: http://json-schema.org/

### HTTP reference example
```
Content-Type: application/json;
          profile="http://example.com/my-hyper-schema#"
```

### JSON Schema example
```json
{
    "title": "Person",
    "type": "object",
    "properties": {
        "firstName": {
            "type": "string"
        },
        "lastName": {
            "type": "string"
        },
        "age": {
            "description": "Age in years",
            "type": "integer",
            "minimum": 0
        }
    },
    "required": ["firstName", "lastName"]
}
```

Source: http://json-schema.org/examples.html

### JSON Schema Core documentation
http://json-schema.org/latest/json-schema-core.html

### JSON Schema Validation documentation
http://json-schema.org/latest/json-schema-validation.html

### JSON Schema IETF Internet Draft (I-D) document
https://github.com/json-schema-org/json-schema-spec

## Schema Guide sources

The online book [Understanding JSON Schema](https://spacetelescope.github.io/understanding-json-schema/index.html) is a great resource for understanding how JSON Schemas can be used. Also available in [PDF](https://spacetelescope.github.io/understanding-json-schema/UnderstandingJSONSchema.pdf).


## Tools

### Suggested schema validator libraries
| Language | Schema version | Repo link |
| ------------- | ------------- | ------------- |
| Java | Draft-4 | https://github.com/java-json-tools/json-schema-validator |
| Swift | Draft-4 |  https://github.com/isimpson/JSONSchema.swift (working repo) |
| Swift | Draft-4 | https://github.com/kylef/JSONSchema.swift (original repo, but has pending merge with required PR) |
| Ruby | Draft-4 | https://github.com/ruby-json-schema/json-schema |
| .Net | Draft-6 | https://www.newtonsoft.com/jsonschema |


### Curated list of schema libraries and tools
http://json-schema.org/implementations.html

### Schema generator using sample JSON as input (online):
https://jsonschema.net/#/editor

### Schema validator (online)
https://jsonschemalint.com/#/version/draft-06/markup/json
