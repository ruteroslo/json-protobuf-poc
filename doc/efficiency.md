# Efficiency

In order to compare JSON and protobuf in terms of efficiency three test cases will be investigated by measuring the time it takes to:

1. produce a large number of JSON and protobuf messages in memory
2. create a large number messages and serialize/deserialize to/from byte array
3. publish the messages to MQTT using QoS 1, and consume them

Notes:

 * The data formats used in the test cases are in NOT the official ITxPT data formats.
 * The protobuf definitions are in proto3.
 * Source code for the tests are found in subdirectory src/efficiency

## Data formats
As messages we will use [a GPS-message (GPS) and an APC-message](docs/message_formats.md) in both JSON and protobuf.

## Message volumes
At Ruter we have 1000 - 1500 busses in operation in the most busy periods of the day.
From each bus we receive GPS-messages every second, and 1-4 APC-messages at every stop. There is on average one stop per minute for many of the routes. The DILAX APC-device also sends a few messages in between stops, so lets assume approx 6 APC-related messages from each bus per minute. Messages will also be produced by the back-end to be consumed by the on board devices. For the sake of this test we will assume one message per second.

This means that there will be approx 126 messages per bus per minute, for 1500 busses this amounts to 189 000 messages per minute, or 3 150 per second.

## Test cases
In each of the test cases we will produce (and consume) messages of each type (GPS and APC) and then compare how efficiently we are able to process the two data formats.

### About the tests
The tests are implemented in Java, and to represent JSON-messages we use Jackson and Lombok annotated Java classes, which is how we would use JSON in a project. With this technique processing JSON messages is very similar to processing protobuf messages in Java. When 'validation' is mentioned in the tests below it means additional validation against a JSON-schema.
Test case 3 makes use of an MQTT-server available on localhost with port 1883 (default).
The tests are run one by one on the command line so that the run time environment is as similar as possible.

### Test case 1: produce a large number of messages
In this test case we simply produce a large number of APC and GPS messages in both JSON and protobuf.
To run each test:

    $ cd src/efficiency
    $ gradle test --rerun-tasks --info --tests no.ruter.TestCase1.should_create_protobuf_messages
    no.ruter.TestCase1 > should_create_protobuf_messages STANDARD_OUT
        Create protobuf messages:
        created 100000 protobuf GPS messages
        created 100000 protobuf APC messages
        created the protobuf messages in 189 milliseconds
        Total message size in bytes: 6860412
        Total message size: 6 MB

    $ gradle test --rerun-tasks --info --tests no.ruter.TestCase1.should_create_json_messages_without_validation
    no.ruter.TestCase1 > should_create_json_messages_without_validation STANDARD_OUT
        Create JSON messages, without validation:
        created 100000 JSON GPS messages
        created 100000 JSON APC messages
        created the JSON messages in 336 milliseconds
        Total message size in bytes: 37482302
        Total message size: 35 MB

    $ gradle test --rerun-tasks --info --tests no.ruter.TestCase1.should_create_json_messages_with_validation
    no.ruter.TestCase1 > should_create_json_messages_with_validation STANDARD_OUT
        Create JSON messages, with validation:
        created 100000 JSON GPS messages
        created 100000 JSON APC messages
        created the JSON messages in 14437 milliseconds
        Total message size in bytes: 37480164
        Total message size: 35 MB

The output from the tests shows that:

* 200 000 protobuf messages can be created in 181 milliseconds.
* 200 000 JSON messsages without validation against a JSON schema can be produced in 341 milliseonds.
* validating against a scheme increases the processing time to 14976 milliseconds (for 200 000 JSON messages.

### Test case 2: create messages and serialize/deserialize to/from byte array
In this test case we create the same messages as in test case 1, and then we serialize each message to a byte array, and thereafter deserialize the byte array back to an object. This test is done without JSON validation against a schema.

To run the tests:

    $ gradle test --rerun-tasks --info --tests no.ruter.TestCase2.should_create_protobuf_messages
    no.ruter.TestCase2 > should_create_protobuf_messages STANDARD_OUT
        Create protobuf messages:
        created 100000 protobuf GPS messages
        created 100000 protobuf APC messages
        created the protobuf messages in 452 milliseconds

    $ gradle test --rerun-tasks --info --tests no.ruter.TestCase2.should_create_json_messages
    no.ruter.TestCase2 > should_create_json_messages STANDARD_OUT
        Create JSON messages:
        created 100000 JSON GPS messages
        created 100000 JSON APC messages
        created the JSON messages in 1163 milliseconds

This shows that protobuf twice as efficiently can be serialized to byte array and deserialized back to protobuf objects, compared to JSON-objects.

### Test case 3: publish to MQTT and consume from MQTT

In this test case we produce 20 000 messages (10 000 APC, 10 000 GPS) in both JSON and protobuf. We produce them to a local MQTT-server and also consume them from the same MQTT-server. This test is done without JSON validation against a schema.

To run the tests:

    $ cd src/efficiency
    $ gradle test --rerun-tasks --info --tests no.ruter.TestCase3.should_create_protobuf_messages_and_produce_and_consume_over_mqtt
    no.ruter.TestCase3 > should_create_protobuf_messages_and_produce_and_consume_over_mqtt STANDARD_OUT
        Create, produce and consume protobuf messages:
        created, produced and consumed 20000 protobuf messages in 6597 milliseconds

    $ gradle test --rerun-tasks --info --tests no.ruter.TestCase3.should_create_json_messages_and_produce_and_consume_over_mqtt
    no.ruter.TestCase3 > should_create_json_messages_and_produce_and_consume_over_mqtt STANDARD_OUT
        Create, produce and consume JSON messages:
        created, produced and consumed 20000 JSON messages in 6855 milliseconds

When actually producing the messages to MQTT and consuming the messages, we see that the overhead in interacting with the server over TCP weighs more than the processing of the messages. Note that in this test case we only produce/consume 20 000 messages.

## Findings
Test cases 1 and 2 show that protobuf is a more efficient format for creating large numbers of messages. However, use case 3 shows that when the messages need to be produced and consumed over MQTT then network IO is more significant than actually creating and serializing/deserializing the messages.
Validation of JSON-messages against a JSON-schema slows down the processing substantially. With protobuf and with JSON implemented with deserialization to Java classes, validation logic can be added as application specific routines instead.
Therefore, for messages to be sent 'over the air', using protobuf does not give a significant advantage compared to using JSON.
