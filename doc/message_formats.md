# Message formats

## protobuf
In this PoC we use protocol buffer version 3 message types for GPS and APC.

### GPS

```
syntax = "proto3";
package no.ruter.ota;

import "google/protobuf/timestamp.proto";

message Gps {

	enum SignalQualityType {
		aGPS = 0;
		dGPS = 1;
		GPSQUALITY = 2;
		ESTIMATEDQUALITY = 3;
		NOTVALID = 4;
		UNKNOWN = 5;
	}

	enum GNSSType {
		GPSTYPE = 0;
		GLONASS = 1;
		GALILEO = 2;
		BEIDOU = 3;
		IRNSS = 4;
		DEADRECKONING = 5;
		MIXEDGNSSTYPE = 6;
		OTHER = 7;
	}

	enum GNSSCoordinateSystem {
		AGPS = 0;
		DGPS = 1;
		ESTIMATED = 2;
		GPS = 3;
		CH1903 = 4;
		ETSR89 = 5;
		IERS = 6;
		NAD27 = 7;
		NAD83 = 8;
		WGS84 = 9;
		WGS72 = 10;
		SGS85 = 11;
		P90 = 12;
// leave these for a later extension:
//		NOTVALID = ;
//		UNKNOWN = ;
	}

	string vehicle_id = 1;
	google.protobuf.Timestamp timestamp_utc = 2;
	float latitude = 3;
	float longitude = 4;
	float speed = 5;
	float course = 6;
	SignalQualityType signal_quality = 7;
	uint32 number_of_satellites = 8;
	GNSSType gnss_type = 9;
	GNSSCoordinateSystem gnss_coordinate_system = 10;

}

```

Kan ikke ha flere enum verdier som er like i samme meldingstype, derfor GSPSQUALITY, GPSTYPE, osv.

### APC

This message type has structures for both door count and vehicle count. In the PoC we will use the door count message.

```
syntax = "proto3";
package no.ruter.ota;

message Apc {
    PassengerDoorCount passenger_door_count = 1;
    PassengerVehicleCount passenger_vehicle_count = 2;
}

message PassengerCount {
    enum PassengerClass {
        ABSENT = 0;
        ADULT = 1;
        CHILD = 2;
        PRAM = 3;
        BIKE = 4;
        WHEELCHAIR = 5;
        OTHER = 6;
    }
    PassengerClass passenger_class = 2;
    uint32 passengers_in = 3;
    uint32 passengers_out = 4;
}

enum CountQuality {
    ABSENT = 0;
    REGULAR = 1;
    DEFECT = 2;
    OTHER = 3;
}


message PassengerDoorCount {
    uint64 recorded_at_time = 1;
    string vehicle_id = 2;
    uint32 door_id = 3;

    repeated PassengerCount passenger_counting = 4;
    CountQuality count_quality = 5;
}

message PassengerVehicleCount {
    uint64 recorded_at_time = 1;
    string vehicle_id = 2;

    // Identifier of stop as defined in AVMS service
    string stop_point_ref = 3;

    repeated PassengerCount passenger_counting = 4;
    CountQuality count_quality = 5;

    uint32 vehicle_occupancy = 6;
    float vehicle_occupancy_ratio = 7;
}
```
## JSON
Below are examples of the JSON GPS- and APC-messages used in the PoC.

### GPS
```
{
  "vehicleId": "328938",
  "timestampUtc": 1508755990,
  "latitude": 59.92362,
  "longitude": 10.57688,
  "speed": 45.6,
  "course": 59.0,
  "signalQuality": "GPS",
  "numberOfSatellites": 7,
  "gnssType" : "GPS",
  "gnssCoordinateSystem" : "WGS84"
}
```

### APC
```
{
  "recordedAtTime" : 1508755990,
  "vehicleId": "328938",
  "doorId": "1",
  "passengerCounts": [
    {
      "passengerClass": "ADULT",
      "passengersIn": 3,
      "passengersOut": 0
    },
    {
      "passengerClass": "CHILD",
      "passengersIn": 2,
      "passengersOut": 1
    }
  ]
}
```
