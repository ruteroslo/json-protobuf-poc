package no.ruter.mqtt;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import no.ruter.models.ApcEvent;
import no.ruter.models.GpsEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import javax.sound.midi.Soundbank;
import java.io.IOException;

/**
 * Created by Bjørn Hjelle, Acando on 04.10.2017.
 */
public class MqttApcJSONConsumer extends MqttConsumer {

    private final static Log LOG = LogFactory.getLog(MqttApcJSONConsumer.class);

    private ApcEvent  message;

    private JsonSchemaFactory jsonSchemaFactory;
    private JsonSchema jsonSchema;


    private ObjectMapper objectMapper;

    long timestamp;

    int count;

    private boolean validate = false;

    public MqttApcJSONConsumer(
            String  topic
            , MqttProperties mqttProperties
            , boolean validate) {
        super(topic, mqttProperties, "ProtobufConsumer");
        objectMapper = new ObjectMapper();
        try {
            jsonSchemaFactory = JsonSchemaFactory.byDefault();
            jsonSchema = jsonSchemaFactory.getJsonSchema("resource:/schemas/apc.json");
        } catch (ProcessingException ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) {
        try {
            message = objectMapper.readValue(mqttMessage.getPayload(), ApcEvent.class);
            if (validate) {
                ProcessingReport report = jsonSchema.validate(objectMapper.convertValue(message, JsonNode.class));
                if (!report.isSuccess()) {
                    System.out.println("Failed to validate!");
                }
            }
            count++;
            timestamp = System.currentTimeMillis();
        } catch (IOException ex) {
            LOG.error(ex.getMessage(), ex);
        } catch (ProcessingException ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    public ApcEvent getApcMessage() {
        return message;
    }
    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
