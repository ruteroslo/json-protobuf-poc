package no.ruter.mqtt;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;

import java.io.File;
import java.time.ZonedDateTime;
import java.util.UUID;

/**
 * Created by bjorn on 27/12/16.
 */
public class MqttProducer implements MqttCallback {

    private final static Log LOG = LogFactory.getLog(MqttProducer.class);

    private MqttClient client;
    private MqttConnectOptions mqttConnectOptions;

    private int count;
    private long timestamp;

    public MqttProducer(MqttProperties mqttProperties) {
        try {
            String dir = mqttProperties.getLockDir();
            dir += File.separator + "lockFile";

            File f = new File(dir);
            if ( !f.exists())
                f.mkdirs();

            MqttDefaultFilePersistence mqttPersistence = new MqttDefaultFilePersistence(dir);
            String uniqueID = mqttProperties.getClientId() + UUID.randomUUID().toString();
            client = new MqttClient(mqttProperties.getServerURI(), uniqueID, mqttPersistence);
            mqttConnectOptions = new MqttConnectOptions();
            mqttConnectOptions.setUserName(mqttProperties.getUsername());
            mqttConnectOptions.setPassword(mqttProperties.getPassword().toCharArray());
            mqttConnectOptions.setCleanSession(true);
            mqttConnectOptions.setMaxInflight(10000);
            client.connect(mqttConnectOptions);
            if (LOG.isDebugEnabled()) {
                LOG.debug("MqttProducer connected:" + client.isConnected());
                LOG.debug("ServerURI:" + mqttProperties.getServerURI());
            }
        } catch (MqttException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public void produce(byte[] payload, String topic, boolean retained) throws MqttException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("connected:");
            LOG.debug(client.isConnected());
            LOG.debug(mqttConnectOptions.getUserName());
        }
        MqttMessage message = new MqttMessage();
        message.setPayload(payload);
        message.setQos(1);
        message.setRetained(retained);
        client.setCallback(this);
        client.publish(topic, message);
    }

    public void close()  throws MqttException {
        client.disconnect();
        client.close();
    }

    private void reconnect() throws MqttException {
        client.connect(mqttConnectOptions);
        client.setCallback(this);
    }

    /**
     *
     * connectionLost
     * This callback is invoked upon losing the MQTT connection.
     *
     */
    @Override
    public void connectionLost(Throwable t) {
        LOG.info("Connection lost!");
        LOG.error(t.getMessage(), t);
        LOG.warn("Lost connection to MQTT server", t);
        while (true) {
            try {
                LOG.info("Attempting to reconnect to MQTT server");
                reconnect();
                LOG.info("Reconnected to MQTT server, resuming");
                return;
            } catch (MqttException e) {
                LOG.warn("Reconnect failed, retrying in 10 seconds", e);
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }

    /**
     *
     * deliveryComplete
     * This callback is invoked when a message published by this client
     * is successfully received by the broker.
     *
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        if (token.isComplete() && token.getException() == null) {
            count++;
            timestamp = System.currentTimeMillis();
//            LOG.debug("received deliveryComplete, message sent successfully");
        } else if ( token.getException() != null) {
            LOG.error(token.getException().getMessage(), token.getException());
        }
    }

    // need to override this in subclasses
    @Override
    public void messageArrived(String topic, MqttMessage message)
            throws Exception {
        LOG.info(message);
    }

    public int getSendCount() {
        return count;
    }


    public long getLastReceivedMilli() {
        return timestamp;
    }

}
