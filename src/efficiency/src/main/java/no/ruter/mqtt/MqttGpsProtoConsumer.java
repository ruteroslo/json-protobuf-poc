package no.ruter.mqtt;

import com.google.protobuf.InvalidProtocolBufferException;
import no.ruter.ota.Apc;
import no.ruter.ota.Gps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by Bjørn Hjelle, Acando on 04.10.2017.
 */
public class MqttGpsProtoConsumer extends MqttConsumer {

    private final static Log LOG = LogFactory.getLog(MqttGpsProtoConsumer.class);

    private Gps.GpsMessage message;



    long timestamp;
    int count;

    public MqttGpsProtoConsumer(
            String  topic
            , MqttProperties mqttProperties) {
        super(topic, mqttProperties, "ProtobufConsumer");
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) {
        try {
            message = Gps.GpsMessage.parseFrom(mqttMessage.getPayload());
            count++;
            timestamp = System.currentTimeMillis();
//            if (apcMessage.hasPassengerDoorCount()) {
//                // do something
//            } else if (apcMessage.hasPassengerVehicleCount()) {
//                // do something else
//            }
        } catch (InvalidProtocolBufferException ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    public Gps.GpsMessage getGpsMessage() {
        return message;
    }
    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
