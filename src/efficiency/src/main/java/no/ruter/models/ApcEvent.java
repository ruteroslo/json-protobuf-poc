package no.ruter.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

/**
 * Created by Bjørn Hjelle, Acando on 24.10.2017.
 */
@Data
@Builder
@AllArgsConstructor
public class ApcEvent {
    private String vehicleId;
    private String doorId;
    private long recordedAtTime;

    @Singular
    private List<PassengerCount> passengerCounts;

    @Data
    @Builder
    @AllArgsConstructor
    public static class PassengerCount {

        private PassengerClass passengerClass;
        private int passengersIn;
        private int passengersOut;

        public enum PassengerClass {
            ABSENT, ADULT, CHILD, PRAM, BIKE, WHEELCHAIR, OTHER;
        }
    }

}
