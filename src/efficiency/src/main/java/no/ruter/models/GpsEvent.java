package no.ruter.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Created by Bjørn Hjelle, Acando on 24.10.2017.
 */
@Data
@AllArgsConstructor
@Builder
public class GpsEvent {
    private String vehicleId;
    private long timestampUtc;
    private float latitude;
    private float longitude;
    private float speed;
    private float course;
    private SignalQualityType signalQuality;
    private short numberOfSatellites;
    private GnssType gnssType;
    private GnssCoordinateSystem gnssCoordinateSystem;

    public enum SignalQualityType {
          AGPS
        , DGPS
        , GPS
        , ESTIMATEDQUALITY
        , NOTVALID
        , UNKNOWN;
    }

    public enum GnssType {
        GPS, GLONASS, GALILEO, BEIDOU, IRNSS, DEADRECKONING, MIXEDGNSSTYPE, OTHER;
    }

    public enum GnssCoordinateSystem {
        AGPS, DGPS, ESTIMATED, GPS, CH1903, ETSR89, IERS, NAD27, NAD83, WGS84, WGS72, SGS85, P90;
    }
}
