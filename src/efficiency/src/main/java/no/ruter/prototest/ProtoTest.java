package no.ruter.prototest;


import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.googlecode.protobuf.format.XmlFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by Bjørn Hjelle, Acando on 03.10.2017.
 */
public abstract class ProtoTest {

//    private final static Log LOG = LogFactory.getLog(ProtoTest.class);
//
//    public static void main(String[] args) {
//        AddressBookProtos.Person person = AddressBookProtos.Person.newBuilder()
//                .setId(1234)
//                .addPhones(AddressBookProtos.Person.PhoneNumber.newBuilder()
//                                .setType(AddressBookProtos.Person.PhoneType.HOME)
//                                .setNumber("+47 913 39 984")
//                                .build()
//                        )
//                .addPhones(AddressBookProtos.Person.PhoneNumber.newBuilder()
//                                .setType(AddressBookProtos.Person.PhoneType.WORK)
//                                .setNumber("+47 222 33 444")
//                                .build()
//                )
//                .setName("Bjørn")
//                .build();
//        System.out.println(person.toString());
//
//        AddressBookProtos.AddressBook.Builder addressBook = AddressBookProtos.AddressBook.newBuilder();
//
//        try {
//            String jsonString = JsonFormat.printer().print(person);
//            System.out.println("json:");
//            System.out.println(jsonString);
//            String xmlString = XmlFormat.printToString(person);
//            System.out.println("xml:");
//            System.out.println(xmlString);
//        } catch (InvalidProtocolBufferException ex) {
//            LOG.error(ex.getMessage(), ex);
//        }
//    }
}
