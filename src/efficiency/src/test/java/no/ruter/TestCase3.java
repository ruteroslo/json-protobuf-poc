package no.ruter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import no.ruter.models.ApcEvent;
import no.ruter.models.GpsEvent;
import no.ruter.mqtt.*;
import no.ruter.ota.Apc;
import no.ruter.ota.Gps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by Bjørn Hjelle, Acando on 24.10.2017.
 */
public class TestCase3 {

   // static Log LOG = LogFactory.getLog(UseCase3.class);

    private MqttProducer mqttProducer;
    private MqttApcProtoConsumer mqttApcProtoConsumer;
    private MqttGpsProtoConsumer mqttGpsProtoConsumer;
    private MqttGpsJSONConsumer mqttGpsJSONConsumer;
    private MqttApcJSONConsumer mqttApcJSONConsumer;
    private MqttProperties mqttProperties;
    private String gpsProtoTopic;
    private String apcProtoTopic;
    private String gpsJSONTopic;
    private String apcJSONTopic;

    final Gps.GpsMessage.SignalQualityType[] signalQuality = {Gps.GpsMessage.SignalQualityType.aGPS};

    private ObjectMapper objectMapper;

//    private int count = 10000;
    private int count = 10;

    private EventFactory eventFactory = new EventFactory();

    @Before
    public void setUp() {
        objectMapper = new ObjectMapper();
        mqttProperties = new MqttProperties();
        mqttProperties.setClientId("protobuf-test");
        mqttProperties.setServerURI("tcp://localhost:1883");
        mqttProperties.setUsername("ruter");
        mqttProperties.setPassword("Ruter2016");
        mqttProducer = new MqttProducer(mqttProperties);
        String topicString = String.valueOf(ZonedDateTime.now().toEpochSecond());
        gpsProtoTopic = topicString + "/gps/proto";
        apcProtoTopic = topicString + "/apc/proto";
        gpsJSONTopic = topicString + "/gps/json";
        apcJSONTopic = topicString + "/apc/json";

        mqttGpsProtoConsumer = new MqttGpsProtoConsumer(gpsProtoTopic, mqttProperties);
        mqttApcProtoConsumer = new MqttApcProtoConsumer(apcProtoTopic, mqttProperties);
        mqttGpsJSONConsumer = new MqttGpsJSONConsumer(gpsJSONTopic, mqttProperties, false);
        mqttApcJSONConsumer = new MqttApcJSONConsumer(apcJSONTopic, mqttProperties, false);

    }

    @Test
    public void should_create_protobuf_messages_and_produce_and_consume_over_mqtt() throws ProcessingException, InterruptedException {
        System.out.println("Create, produce and consume protobuf messages:");

        long before = System.currentTimeMillis();

        List<Gps.GpsMessage> protoGpsMessages = eventFactory.createRandomProtobufGpsMessages(count);
        List<Apc.ApcMessage> protoApcMessages =  eventFactory.createRandomProtobufApcMessages(count);

        try {
            for (Gps.GpsMessage message : protoGpsMessages) {
               mqttProducer.produce(message.toByteArray(), gpsProtoTopic, false);
            }
            for (Apc.ApcMessage message : protoApcMessages) {
                mqttProducer.produce(message.toByteArray(), apcProtoTopic, false);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.exit(1);
        }

        Thread.sleep(1000);

        assertThat(mqttProducer.getSendCount()).isEqualTo(protoGpsMessages.size() + protoApcMessages.size());

        assertThat(mqttGpsProtoConsumer.getCount()).isEqualTo(protoGpsMessages.size());
        assertThat(mqttApcProtoConsumer.getCount()).isEqualTo(protoApcMessages.size());

        long afterGps = mqttGpsProtoConsumer.getTimestamp();
        long afterApc = mqttApcProtoConsumer.getTimestamp();
        long after = Math.max(afterApc, afterGps);

        System.out.println(String.format("created, produced and consumed %d protobuf messages in %d milliseconds", count * 2, after - before));

    }

    @Test
    public void should_create_json_messages_and_produce_and_consume_over_mqtt() throws ProcessingException, InterruptedException {
        System.out.println("Create, produce and consume JSON messages:");

        long before = System.currentTimeMillis();

        List<GpsEvent> jsonGpsMessages = eventFactory.createRandomJsonGpsMessages(count, false);
        List<ApcEvent> jsonApcMessages =  eventFactory.createRandomJsonApcMessages(count, false);


        try {
            for (GpsEvent message : jsonGpsMessages) {
                mqttProducer.produce(objectMapper.writeValueAsBytes(message), gpsJSONTopic, false);
            }
            for (ApcEvent message : jsonApcMessages) {
                mqttProducer.produce(objectMapper.writeValueAsBytes(message), apcJSONTopic, false);
            }
        } catch (MqttException ex) {
            System.out.println(ex.getMessage());
            System.exit(1);
        } catch (JsonProcessingException ex) {
            System.out.println(ex.getMessage());
            System.exit(1);
        }

        Thread.sleep(1000);

        assertThat(mqttProducer.getSendCount()).isEqualTo(jsonGpsMessages.size() + jsonApcMessages.size());

        assertThat(mqttGpsJSONConsumer.getCount()).isEqualTo(jsonGpsMessages.size());
        assertThat(mqttApcJSONConsumer.getCount()).isEqualTo(jsonApcMessages.size());

        long afterGps = mqttGpsJSONConsumer.getTimestamp();
        long afterApc = mqttApcJSONConsumer.getTimestamp();
        long after = Math.max(afterApc, afterGps);

        System.out.println(String.format("created, produced and consumed %d JSON messages in %d milliseconds", count * 2, after - before));

    }

}
