package no.ruter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import no.ruter.models.ApcEvent;
import no.ruter.models.GpsEvent;
import no.ruter.ota.Apc;
import no.ruter.ota.Gps;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by Bjørn Hjelle, Acando on 24.10.2017.
 */
public class TestCase1 {

    final Gps.GpsMessage.SignalQualityType[] signalQuality = {Gps.GpsMessage.SignalQualityType.aGPS};

    private int count = 100_000;
//    private int count = 10000;

    private EventFactory eventFactory = new EventFactory();

    @Before
    public void setUp() {

    }

    @Test
    public void should_create_protobuf_messages() throws ProcessingException {
        System.out.println("Create protobuf messages:");

        long before = System.currentTimeMillis();

        List<Gps.GpsMessage> protoGpsMessages = eventFactory.createRandomProtobufGpsMessages(count);
        System.out.println(String.format("created %d protobuf GPS messages", protoGpsMessages.size()));
        List<Apc.ApcMessage> protoApcMessages =  eventFactory.createRandomProtobufApcMessages(count);
        System.out.println(String.format("created %d protobuf APC messages", protoGpsMessages.size()));

        long after = System.currentTimeMillis();

        System.out.println(String.format("created the protobuf messages in %d milliseconds", after - before));
        long byteCount = protoGpsMessages.stream().mapToInt(Gps.GpsMessage::getSerializedSize).sum()
                + protoApcMessages.stream().mapToInt(Apc.ApcMessage::getSerializedSize).sum();

        System.out.println(String.format("Total message size in bytes: %d", byteCount));
        System.out.println(String.format("Total message size: %s",
                FileUtils.byteCountToDisplaySize(byteCount)));
    }


    @Test
    public void should_create_json_messages_without_validation() throws ProcessingException {
        System.out.println("Create JSON messages, without validation:");
        create_json_messages(false);
    }

    @Test
    public void should_create_json_messages_with_validation() throws ProcessingException {
        System.out.println("Create JSON messages, with validation:");
        create_json_messages(true);
    }


    private void create_json_messages(boolean validate)  throws ProcessingException {


        long before = System.currentTimeMillis();

        List<GpsEvent> jsonGpsMessages = eventFactory.createRandomJsonGpsMessages(count, validate);
        System.out.println(String.format("created %d JSON GPS messages", jsonGpsMessages.size()));
        List<ApcEvent> jsonApcMessages = eventFactory.createRandomJsonApcMessages(count, validate);
        System.out.println(String.format("created %d JSON APC messages", jsonApcMessages.size()));

        long after = System.currentTimeMillis();

        System.out.println(String.format("created the JSON messages in %d milliseconds", after - before));

        ObjectMapper objectMapper = new ObjectMapper();
        long byteCount = 0;
        try {
            for (int i = 0; i < jsonGpsMessages.size(); i++) {
                byteCount += objectMapper.writer().writeValueAsBytes(jsonGpsMessages.get(i)).length;
                byteCount += objectMapper.writer().writeValueAsBytes(jsonApcMessages.get(i)).length;
            }
        } catch (JsonProcessingException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println(String.format("Total message size in bytes: %d", byteCount));
        System.out.println(String.format("Total message size: %s",
                FileUtils.byteCountToDisplaySize(byteCount)));


    };


}
