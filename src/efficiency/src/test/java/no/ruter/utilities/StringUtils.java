package no.ruter.utilities;

import java.io.InputStream;
import java.util.Optional;

/**
 * @author Bjørn Hjelle, Acando on 05.10.2016.
 */
public final class StringUtils {

    private StringUtils() {
        //util class
    }

    public static InputStream getResourceAsStream(String path) {
        return getOptionalResourceAsStream(path).orElseThrow(() -> new IllegalArgumentException("Resource: " + path + " does not exist."));
    }

    public static Optional<InputStream> getOptionalResourceAsStream(String path) {
        return Optional.ofNullable(
                StringUtils.class
                        .getClassLoader()
                        .getResourceAsStream(path)
        );
    }

    public static String getResourceAsString(String path) {
        return getOptionalResourceAsStream(path).map(StringUtils::convertStreamToString)
                .orElseThrow(() -> new IllegalArgumentException("Resource: " + path + " does not exist."));
    }


    public static String convertStreamToString(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}