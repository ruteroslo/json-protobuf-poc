package no.ruter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.google.protobuf.InvalidProtocolBufferException;
import no.ruter.models.ApcEvent;
import no.ruter.models.GpsEvent;
import no.ruter.ota.Apc;
import no.ruter.ota.Gps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Created by Bjørn Hjelle, Acando on 24.10.2017.
 */
public class TestCase2 {

    //static Log LOG = LogFactory.getLog(UseCase2.class);

    final Gps.GpsMessage.SignalQualityType[] signalQuality = {Gps.GpsMessage.SignalQualityType.aGPS};

    private int count = 100_000;

    private EventFactory eventFactory = new EventFactory();

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
    }

    @Test
    public void should_create_protobuf_messages() throws ProcessingException, InvalidProtocolBufferException {
        System.out.println("Create protobuf messages:");

        long before = System.currentTimeMillis();

        List<Gps.GpsMessage> protoGpsMessages = eventFactory.createRandomProtobufGpsMessages(count);
        System.out.println(String.format("created %d protobuf GPS messages", protoGpsMessages.size()));
        List<Apc.ApcMessage> protoApcMessages =  eventFactory.createRandomProtobufApcMessages(count);
        System.out.println(String.format("created %d protobuf APC messages", protoGpsMessages.size()));

        // convert to byte array and parse from byte array
        byte[] bytes;
        for (Gps.GpsMessage message : protoGpsMessages) {
            bytes = message.toByteArray();
            Gps.GpsMessage tmp = Gps.GpsMessage.parseFrom(bytes);
        }
        for (Apc.ApcMessage message : protoApcMessages) {
            bytes = message.toByteArray();
            Apc.ApcMessage tmp = Apc.ApcMessage.parseFrom(bytes);
        }

        long after = System.currentTimeMillis();

        System.out.println(String.format("created the protobuf messages in %d milliseconds", after - before));
    }

    @Test
    public void should_create_json_messages() throws ProcessingException, JsonProcessingException, IOException {


        System.out.println("Create JSON messages:");

        long before = System.currentTimeMillis();

        List<GpsEvent> jsonGpsMessages = eventFactory.createRandomJsonGpsMessages(count, false);
        System.out.println(String.format("created %d JSON GPS messages", jsonGpsMessages.size()));
        List<ApcEvent> jsonApcMessages = eventFactory.createRandomJsonApcMessages(count, false);
        System.out.println(String.format("created %d JSON APC messages", jsonApcMessages.size()));

        byte[] bytes;
        for (GpsEvent message : jsonGpsMessages) {
            bytes = objectMapper.writeValueAsBytes(message);
            GpsEvent tmp = objectMapper.readValue(bytes, GpsEvent.class);
        }

        for (ApcEvent message : jsonApcMessages) {
            bytes = objectMapper.writeValueAsBytes(message);
            ApcEvent tmp = objectMapper.readValue(bytes, ApcEvent.class);
        }

        long after = System.currentTimeMillis();

        System.out.println(String.format("created the JSON messages in %d milliseconds", after - before));



    };


}
