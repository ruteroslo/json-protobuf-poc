package no.ruter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.google.protobuf.Timestamp;
import no.ruter.models.ApcEvent;
import no.ruter.models.GpsEvent;
import no.ruter.ota.Apc;
import no.ruter.ota.Gps;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Bjørn Hjelle, Acando on 04.10.2017.
 */
public class EventFactory {
    private final Random ran = new Random();

    String[] vehicleIDs = {"123456", "212345", "312345", "412345", "512345", "612345", "712345"};
    final int VEHICLEIDS_LENGTH = vehicleIDs.length - 1;

    Gps.GpsMessage.GNSSType[] gnssTypes = Gps.GpsMessage.GNSSType.values();
    final int GNSS_TYPES_LENGTH = gnssTypes.length - 1;

    Gps.GpsMessage.GNSSCoordinateSystem[] coordinateSystems = Gps.GpsMessage.GNSSCoordinateSystem.values();
    final int GNSS_COORDINATE_SYSTEMS_LENGTH = coordinateSystems.length - 1;

    Gps.GpsMessage.SignalQualityType[] signalQualityTypes = Gps.GpsMessage.SignalQualityType.values();
    final int SIGNAL_QUALITY_TYPES_LENGTH = signalQualityTypes.length - 1;

    Apc.CountQuality[] countQualities = Apc.CountQuality.values();
    final int COUNT_QUALITIES_LENGTH = countQualities.length - 1;

    Apc.PassengerCount.PassengerClass[] passengerClasses = Apc.PassengerCount.PassengerClass.values();
    final int PASSENGER_CLASSES_LENGTH = passengerClasses.length - 1;


    GpsEvent.SignalQualityType[] jsonSignalQualityTypes = GpsEvent.SignalQualityType.values();
    final int JSON_SIGNAL_QUALITY_TYPES_LENGTH = jsonSignalQualityTypes.length;

    GpsEvent.GnssType[] jsonGnssTypes = GpsEvent.GnssType.values();
    final int JSON_GNSS_TYPES_LENGTH = jsonGnssTypes.length;

    GpsEvent.GnssCoordinateSystem[] jsonGnssCoordinateSystems = GpsEvent.GnssCoordinateSystem.values();
    final int JSON_GNSS_COORDINATE_SYSTEMS_LENGTH = jsonGnssCoordinateSystems.length;

    ApcEvent.PassengerCount.PassengerClass[] jsonPassengerClasses = ApcEvent.PassengerCount.PassengerClass.values();
    final int JSON_PASSENGER_CLASSES_LENGTH = jsonPassengerClasses.length;

    String[] stringGnssTypes = {"GPS", "GLONASS", "GALILEO", "BEIDOU", "IRNSS", "DEADRECKONING", "MIXEDGNSSTYPE", "OTHER"};
    final int STRING_GNSS_TYPES_LENGTH = stringGnssTypes.length;

    String[] stringGnssCoordinateSystems = {"AGPS", "DGPS", "ESTIMATED", "GPS", "CH1903", "ETSR89", "IERS", "NAD27", "NAD83", "WGS84", "WGS72", "SGS85", "P90"};
    final int STRING_GNSS_COORDINATE_SYSTEMS_LENGTH = stringGnssCoordinateSystems.length;

    String[] stringSignalQualityTypes = {"AGPS", "DGPS", "GPS", "ESTIMATEDQUALITY", "NOTVALID", "UNKNOWN"};
    final int STRING_SIGNAL_QUALITY_TYPES_LENGTH = stringSignalQualityTypes.length;

    String[] stringPassengerClasses = {"ABSENT", "ADULT", "CHILD", "PRAM", "BIKE", "WHEELCHAIR", "OTHER"};
    final int STRING_PASSENGER_CLASSES_LENGTH = stringPassengerClasses.length;

    private ObjectMapper objectMapper = new ObjectMapper();

    public List<Gps.GpsMessage> createRandomProtobufGpsMessages(int count) {
        List<Gps.GpsMessage> list = new ArrayList<>();

        long now = ZonedDateTime.now().toEpochSecond();

        for (int i=0; i < count; i++) {
            // (.length -1) because proto3 compiler adds "UNRECOGNIZED" to enum
            Gps.GpsMessage.GNSSType gnssType = gnssTypes[ran.nextInt(GNSS_TYPES_LENGTH)];
            Gps.GpsMessage.GNSSCoordinateSystem coordinateSystem = coordinateSystems[ran.nextInt(GNSS_COORDINATE_SYSTEMS_LENGTH)];
            Gps.GpsMessage.SignalQualityType signalQualityType = signalQualityTypes[ran.nextInt(SIGNAL_QUALITY_TYPES_LENGTH)];
            list.add(Gps.GpsMessage.newBuilder()
                    .setVehicleId(vehicleIDs[ran.nextInt(VEHICLEIDS_LENGTH)])
                    .setTimestampUtc(Timestamp.newBuilder()
                            .setSeconds(now).build())
                    .setLatitude(56.0F + (62.4F - 56.0F) * ran.nextFloat())
                    .setLongitude(8.1F + (12.4F - 8.1F) * ran.nextFloat())
                    .setCourse(360F * ran.nextFloat())
                    .setSpeed(80F * ran.nextFloat())
                    .setGnssType(gnssType)
                    .setGnssCoordinateSystem(coordinateSystem)
                    .setSignalQuality(signalQualityType)
                    .build());
        }
        return list;
    }

    public List<Apc.ApcMessage> createRandomProtobufApcMessages(int count) {
        List<Apc.ApcMessage> list = new ArrayList<>();
        long now = ZonedDateTime.now().toEpochSecond();
        for (int i=0; i < count; i++) {
            Apc.CountQuality countQuality = countQualities[ran.nextInt(COUNT_QUALITIES_LENGTH)];
            list.add(Apc.ApcMessage.newBuilder()
                    .setPassengerDoorCount(
                            Apc.PassengerDoorCount.newBuilder()
                                    .setRecordedAtTime(now)
                                    .setDoorId(1)
                                    .setCountQuality(countQuality)
                                    .addPassengerCounting(Apc.PassengerCount.newBuilder()
                                            .setPassengerClass(
                                                    Apc.PassengerCount.PassengerClass.ADULT)
                                            .setPassengersIn(ran.nextInt(50))
                                            .setPassengersOut(ran.nextInt(50))
                                            .build())
                                    .addPassengerCounting(Apc.PassengerCount.newBuilder()
                                            .setPassengerClass(
                                                    Apc.PassengerCount.PassengerClass.CHILD)
                                            .setPassengersIn(ran.nextInt(50))
                                            .setPassengersOut(ran.nextInt(50))
                                            .build())
                                    .build())
                    .build());
        }
        return list;
    }

    public List<GpsEvent> createRandomJsonGpsMessages(int count, boolean validate) throws ProcessingException {
        List<GpsEvent> list = new ArrayList<>();

        final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        final JsonSchema schema = factory.getJsonSchema("resource:/schemas/gps.json");

        assertThat(schema).isNotNull();
        long now = ZonedDateTime.now().toEpochSecond();

        for (int i=0; i < count; i++) {
            list.add(GpsEvent.builder()
                    .vehicleId(vehicleIDs[ran.nextInt(VEHICLEIDS_LENGTH)])
                    .timestampUtc(now)
                    .latitude(56.0F + (62.4F - 56.0F) * ran.nextFloat())
                    .longitude(8.1F + (12.4F - 8.1F) * ran.nextFloat())
                    .course(360F * ran.nextFloat())
                    .speed(80F * ran.nextFloat())
                    .gnssType(jsonGnssTypes[ran.nextInt(JSON_GNSS_TYPES_LENGTH)])
                    .gnssCoordinateSystem(jsonGnssCoordinateSystems[ran.nextInt(JSON_GNSS_COORDINATE_SYSTEMS_LENGTH)])
                    //.signalQuality(stringSignalQualityTypes[ran.nextInt(STRING_SIGNAL_QUALITY_TYPES_LENGTH)])
                    .signalQuality(jsonSignalQualityTypes[ran.nextInt(JSON_SIGNAL_QUALITY_TYPES_LENGTH)])
                    .build());

            if (validate) {
                ProcessingReport report = schema.validate(objectMapper.convertValue(list.get(i), JsonNode.class));
                if (!report.isSuccess()) {
                    System.out.println("Failed to validate!");
                }
            }


        }
        return list;
    }

    public List<ApcEvent> createRandomJsonApcMessages(int count, boolean validate) throws ProcessingException{
        List<ApcEvent> list = new ArrayList<>();

        final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        final JsonSchema schema = factory.getJsonSchema("resource:/schemas/apc.json");

        ObjectMapper objectMapper = new ObjectMapper();
        long now = ZonedDateTime.now().toEpochSecond();

        for (int i=0; i < count; i++) {
            list.add(ApcEvent.builder()
                    .vehicleId(vehicleIDs[ran.nextInt(VEHICLEIDS_LENGTH)])
                    .doorId("1")
                    .recordedAtTime(now)
                    .passengerCount(ApcEvent.PassengerCount.builder()
                            .passengerClass(jsonPassengerClasses[ran.nextInt(JSON_PASSENGER_CLASSES_LENGTH)])
                            .passengersIn(ran.nextInt(50))
                            .passengersOut(ran.nextInt(50))
                            .build())
                    .build());

            if (validate) {

                ProcessingReport report = schema.validate(objectMapper.convertValue(list.get(i), JsonNode.class));

                if (!report.isSuccess()) {
                    System.out.println("Failed to validate!");
                    System.out.println(report.toString());
                }
            }


        }
        return list;
    }

 }
