package no.ruter;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.ruter.models.GpsEvent;
import no.ruter.utilities.StringUtils;
import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Created by Bjørn Hjelle, Acando on 24.10.2017.
 */
public class GpsEventTest {

    @Test
    public void should_be_able_to_deserialize_gps_event() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        GpsEvent event = mapper.readValue(StringUtils.getResourceAsStream("json/gps.json"), GpsEvent.class);
        assertThat(event).isNotNull();
    }

}
