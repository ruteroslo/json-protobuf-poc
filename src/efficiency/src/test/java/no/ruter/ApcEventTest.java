package no.ruter;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.ruter.models.ApcEvent;
import no.ruter.models.GpsEvent;
import no.ruter.utilities.StringUtils;
import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Created by Bjørn Hjelle, Acando on 24.10.2017.
 */
public class ApcEventTest {

    @Test
    public void should_be_able_to_deserialize_apc_event() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ApcEvent event = mapper.readValue(StringUtils.getResourceAsStream("json/apc.json"), ApcEvent.class);
        assertThat(event).isNotNull();
    }
}
