package no.ruter;


import com.fasterxml.jackson.databind.ObjectMapper;
import no.ruter.mqtt.MqttApcJSONConsumer;
import no.ruter.mqtt.MqttApcProtoConsumer;
import no.ruter.mqtt.MqttProperties;

/**
 * Created by Bjørn Hjelle, Acando on 03.10.2017.
 */
public class ConsumeProtoAndJSON {
    private MqttApcProtoConsumer mqttApcProtoConsumer;
    private MqttApcJSONConsumer mqttApcJSONConsumer;
    private MqttProperties mqttProperties;
    private String gpsProtoTopic;
    private String apcProtoTopic;
    private String gpsJSONTopic;
    private String apcJSONTopic;

    private ObjectMapper objectMapper;

    public ConsumeProtoAndJSON() {
        objectMapper = new ObjectMapper();
        mqttProperties = new MqttProperties();
        mqttProperties.setClientId("protobuf-test");
        mqttProperties.setServerURI("tcp://localhost:1883");
        mqttProperties.setUsername("ruter");
        mqttProperties.setPassword("Ruter2016");
        String topicString = "consumeproducetest";
        apcProtoTopic = topicString + "/apc/proto";
        apcJSONTopic = topicString + "/apc/json";

        mqttApcProtoConsumer = new MqttApcProtoConsumer(apcProtoTopic, mqttProperties);
        mqttApcJSONConsumer = new MqttApcJSONConsumer(apcJSONTopic, mqttProperties, true);
    }



    public static void main(String[] args ) throws InterruptedException{
       ConsumeProtoAndJSON consumer = new ConsumeProtoAndJSON();
       while (true) {
           Thread.sleep(1000);
       }
    }

}
