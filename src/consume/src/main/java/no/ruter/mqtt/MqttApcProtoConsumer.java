package no.ruter.mqtt;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import no.ruter.ota.Apc;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by Bjørn Hjelle, Acando on 04.10.2017.
 */
public class MqttApcProtoConsumer extends MqttConsumer {

    private final static Log LOG = LogFactory.getLog(MqttApcProtoConsumer.class);

    private Apc.ApcMessage apcMessage;



    long timestamp;
    int count;

    public MqttApcProtoConsumer(
            String  topic
            , MqttProperties mqttProperties) {
        super(topic, mqttProperties, "ProtobufConsumer");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        try {
            apcMessage = Apc.ApcMessage.parseFrom(message.getPayload());
            count++;
            timestamp = System.currentTimeMillis();
            System.out.println("messageArrived");
            System.out.println(JsonFormat.printer().includingDefaultValueFields().print(apcMessage));
//            if (apcMessage.hasPassengerDoorCount()) {
//                // do something
//            } else if (apcMessage.hasPassengerVehicleCount()) {
//                // do something else
//            }
        } catch (InvalidProtocolBufferException ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    public Apc.ApcMessage getApcMessage() {
        return apcMessage;
    }
    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
