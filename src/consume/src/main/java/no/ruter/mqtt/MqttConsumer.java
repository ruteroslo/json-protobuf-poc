package no.ruter.mqtt;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;

import java.io.File;
import java.util.UUID;

/**
 * Created by bjorn on 27/12/16.
 */
public class MqttConsumer implements MqttCallback {

    private final static Log LOG = LogFactory.getLog(MqttConsumer.class);

    private MqttClient client;
    private String[] topics;
    private MqttConnectOptions mqttConnectOptions;

    public MqttConsumer() {}

    public MqttConsumer(String topic
            , MqttProperties mqttProperties
            , String consumerName) {
        this(new String[]{topic}, mqttProperties, consumerName);
    }
    public MqttConsumer(String[] topics
            , MqttProperties mqttProperties
            , String consumerName) {
        this.topics = topics;

        MqttDefaultFilePersistence mqttPersistence = createFilePersistence(mqttProperties);

        try {
            client = new MqttClient(mqttProperties.getServerURI(), consumerName + "-" + UUID.randomUUID(), mqttPersistence);
            mqttConnectOptions = new MqttConnectOptions();
            mqttConnectOptions.setUserName(mqttProperties.getUsername());
            mqttConnectOptions.setPassword(mqttProperties.getPassword().toCharArray());
            mqttConnectOptions.setCleanSession(true);
            client.connect(mqttConnectOptions);
            client.setCallback(this);
            System.out.println("will subscribe to topic:" + topics[0]);
            client.subscribe(topics);
        } catch (MqttException ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }


    private void reconnect() throws MqttException {
        client.connect(mqttConnectOptions);
        client.setCallback(this);
        LOG.debug("will subscribe again...");
        client.subscribe(topics);
    }

    /**
     * connectionLost
     * This callback is invoked upon losing the MQTT connection.
     */
    @Override
    public void connectionLost(Throwable t) {
        LOG.info("Connection lost!");
        LOG.error(t.getMessage(), t);
        LOG.warn("Lost connection to MQTT server", t);
        while (true) {
            try {
                LOG.info("Attempting to reconnect to MQTT server");
                reconnect();
                LOG.info("Reconnected to MQTT server, resuming");
                return;
            } catch (MqttException e) {
                LOG.warn("Reconnect failed, retrying in 10 seconds", e);
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }

    /**
     * messageArrived
     * This callback is invoked when a message is received on a subscribed topic.
     */
    @Override
    public void messageArrived(String topic, MqttMessage message) {
        LOG.debug("-------------Received message -------------------------");
        try {
            LOG.debug("topic: " + topic);
            LOG.debug(new String(message.getPayload()));
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * deliveryComplete
     * This callback is invoked when a message published by this client
     * is successfully received by the broker.
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        LOG.debug("delivered message: " + token.isComplete());
        //System.out.println("Pub complete" + new String(token.getMessage().getPayload()));
    }

    private MqttDefaultFilePersistence createFilePersistence(MqttProperties mqttProperties) {

        LOG.debug("create lock directory: " + mqttProperties.getLockDir());
        String dir = mqttProperties.getLockDir();
        dir += File.separator +"lockFiles";

        File f = new File(dir);
        if(!f.exists())
                f.mkdirs();
        if(f.exists()) {
            LOG.debug("lock directory exists");
        } else {
            LOG.debug("lock directory does not exist");
        }

        return new MqttDefaultFilePersistence(dir);
    }

}
