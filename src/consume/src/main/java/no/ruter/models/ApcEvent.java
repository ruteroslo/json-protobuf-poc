package no.ruter.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

/**
 * Created by Bjørn Hjelle, Acando on 24.10.2017.
 */
@Data
@Builder
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApcEvent {
    private String vehicleId;
    private String doorId;
    private long recordedAtTime;
    private CountQuality countQuality;


    public enum CountQuality {
        ABSENT, REGULAR, DEFECT, OTHER;
    }

    @Singular
    private List<PassengerCount> passengerCounts;

    @Data
    @Builder
    @AllArgsConstructor
    public static class PassengerCount {

        private PassengerClass passengerClass;
        private int PassengersIn;
        private int PassengersOut;

        public enum PassengerClass {
            ABSENT, ADULT, CHILD, PRAM, BIKE, WHEELCHAIR, OTHER;
        }
    }

}
