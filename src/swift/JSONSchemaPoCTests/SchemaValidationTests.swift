//
//  JSONSchemaPoCTests.swift
//  JSONSchemaPoCTests
//
//  Created by Per-Ove Joakimsen on 24/10/2017.
//  Copyright © 2017 Knowit. All rights reserved.
//

import XCTest
import JSONSchema

@testable import JSONSchemaPoC

class SchemaValidationTests: XCTestCase {
    
    fileprivate enum DataType {
        case gps, apc
    }

    fileprivate let testIterations = 1000
    fileprivate let jsonDirectory = "json"
    fileprivate let schemaDirectory = "schemas"

    fileprivate func loadDocument(dataType: DataType) -> [String: Any] {
        return loadJSON(fileName: "\(dataType)", fileType: "json", directory: jsonDirectory)
    }
    
    fileprivate func loadSchema(dataType: DataType) -> Schema {
        return Schema(loadJSON(fileName: "\(dataType)", fileType: "json", directory: schemaDirectory))

    }

    fileprivate func loadJSON(fileName: String, fileType: String, directory: String) -> [String: Any] {
        let testBundle = Bundle(for: type(of: self))
        if let path = testBundle.path(forResource: fileName, ofType: fileType, inDirectory: directory) {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                return json as! [String: Any]
            } catch {
                // handle error
            }
        }
        return [String: Any]()
    }
    
    fileprivate func createGPSDocument() -> [String: Any] {
        return [
            "vehicleId": "328938",
            "timestampUtc": 1508755990,
            "latitude": 59.92362,
            "longitude": 10.57688,
            "speed": 45.6,
            "course": 59.0,
            "signalQuality": "GPS",
            "numberOfSatellites": 7,
            "gnssType": "GPS",
            "gnssCoordinateSystem": "WGS84"
        ]
    }
    
    fileprivate func createAPCDocument() -> [String: Any] {
        return [
            "recordedAtTime" : 1508755990,
            "vehicleId": "328938",
            "doorId": "1",
            "passengerCounts": [
                [
                    "passengerClass": "ADULT",
                    "passengersIn": 3,
                    "passengersOut": 3
                    ],
                [
                    "passengerClass": "ADULT",
                    "passengersIn": 3,
                    "passengersOut": 3
                    ]
            ]
        ]
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: - GPS
    
    func testGPSDocumentAndSchemaLoading() {
        let document = loadJSON(fileName: "gps", fileType: "json", directory: jsonDirectory)
        XCTAssertNotNil(document)
        let schema = loadJSON(fileName: "gps", fileType: "json", directory: schemaDirectory)
        XCTAssertNotNil(schema)
    }
    
    func testGPSDocumentAndSchemaCorrectValidation() {
        let document = self.loadDocument(dataType: .gps)
        let schema = self.loadSchema(dataType: .gps)
        let result = schema.validate(document)
        XCTAssert(result.errors == nil)
    }
    
    func testGPSDocumentAndSchemaWithEnumFaultValidation() {
        var document = self.createGPSDocument()
        document["gnssCoordinateSystem"] = "FAULTY"
        let schema = self.loadSchema(dataType: .gps)
        let result = schema.validate(document)
        XCTAssertNotNil(result.errors)
    }
    
    func testGPSDocumentCreationPerf() {
        self.measure {
            for _ in 0..<testIterations {
                let _ = self.createGPSDocument()
            }
        }
    }
    
    func testGPSDocumentCreationAndValidationPerf() {
        let schema = self.loadSchema(dataType: .gps)
        self.measure {
            for _ in 0..<testIterations {
                let json = self.createGPSDocument()
                _ = schema.validate(json)
            }
        }
    }

    
    // MARK: - APC
    
    func testAPCDocumentAndSchemaLoading() {
        let document = loadJSON(fileName: "apc", fileType: "json", directory: jsonDirectory)
        XCTAssertNotNil(document)
        let schema = loadJSON(fileName: "apc", fileType: "json", directory: schemaDirectory)
        XCTAssertNotNil(schema)
    }
    
    func testAPCDocumentAndSchemaCorrectValidation() {
        let document = self.loadDocument(dataType: .apc)
        let schema = self.loadSchema(dataType: .apc)
        let result = schema.validate(document)
        XCTAssert(result.errors == nil, "\(String(describing: result.errors))")
    }
    
    func testAPCDocumentAndSchemaWithEnumFaultValidation() {
        var document = self.createAPCDocument()
        if var passangerCounts = document["passengerCounts"] as? [[String: Any]] {
            passangerCounts[0]["passengerClass"] = "FAULTY"
            document["passengerCounts"] = passangerCounts
        }
        let schema = self.loadSchema(dataType: .apc)
        let result = schema.validate(document)
        XCTAssertNotNil(result.errors)
    }
    
    func testAPCDocumentCreationPerf() {
        self.measure {
            for _ in 0..<testIterations {
                let _ = self.createAPCDocument()
            }
        }
    }
    
    func testAPCDocumentCreationAndValidationPerf() {
        let schema = self.loadSchema(dataType: .apc)
        self.measure {
            for _ in 0..<testIterations {
                let json = self.createAPCDocument()
                _ = schema.validate(json)
            }
        }
    }

}
