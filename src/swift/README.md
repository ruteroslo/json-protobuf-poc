# Notes

## Data and schema dependencies
Tests uses json documents and schema from root source (referenced in project file).

## Cocoapods dependencies

Uses forked version of the original JSONSchema repo (check [Podfile](Podfile) for details).

Run `pod update` to get or update pod(s)
