package no.ruter;


import com.fasterxml.jackson.databind.ObjectMapper;
import no.ruter.mqtt.MqttProducer;
import no.ruter.mqtt.MqttProperties;
import no.ruter.ota.Apc;

import java.util.List;

/**
 * Created by Bjørn Hjelle, Acando on 03.10.2017.
 */
public class ProduceProto {

    private MqttProducer mqttProducer;
    private MqttProperties mqttProperties;
    private int count = 10;

    private EventFactory eventFactory = new EventFactory();

    public ProduceProto() {
        mqttProperties = new MqttProperties();
        mqttProperties.setClientId("protobuf-test");
        mqttProperties.setServerURI("tcp://localhost:1883");
        mqttProperties.setUsername("ruter");
        mqttProperties.setPassword("Ruter2016");
        mqttProducer = new MqttProducer(mqttProperties);

    }

    public void run() throws Exception {
        List<Apc.ApcMessage> protoApcMessages =  eventFactory.createRandomProtobufApcMessages(count);

        for (Apc.ApcMessage message : protoApcMessages) {
            mqttProducer.produce(message.toByteArray(), "consumeproducetest/apc/proto", false);
        }
        mqttProducer.close();

    }

    public static void main(String[] args ) throws Exception{
       ProduceProto producer = new ProduceProto();
       producer.run();
    }

}
