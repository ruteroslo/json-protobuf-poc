package no.ruter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import no.ruter.models.ApcEvent;
import no.ruter.ota.Apc;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by Bjørn Hjelle, Acando on 04.10.2017.
 */
public class EventFactory {
    private final Random ran = new Random();

    String[] vehicleIDs = {"123456", "212345", "312345", "412345", "512345", "612345", "712345"};
    final int VEHICLEIDS_LENGTH = vehicleIDs.length - 1;

    Apc.CountQuality[] countQualities = Apc.CountQuality.values();
    final int COUNT_QUALITIES_LENGTH = countQualities.length - 1;

    Apc.PassengerCount.PassengerClass[] passengerClasses = Apc.PassengerCount.PassengerClass.values();
    final int PASSENGER_CLASSES_LENGTH = passengerClasses.length - 1;



    ApcEvent.PassengerCount.PassengerClass[] jsonPassengerClasses = ApcEvent.PassengerCount.PassengerClass.values();
    final int JSON_PASSENGER_CLASSES_LENGTH = jsonPassengerClasses.length;

    ApcEvent.CountQuality[] jsonCountQualities = ApcEvent.CountQuality.values();
    final int JSON_COUNT_QUALITIES_LENGTH = jsonCountQualities.length;

    String[] stringPassengerClasses = {"ABSENT", "ADULT", "CHILD", "PRAM", "BIKE", "WHEELCHAIR", "OTHER"};
    final int STRING_PASSENGER_CLASSES_LENGTH = stringPassengerClasses.length;

    private ObjectMapper objectMapper = new ObjectMapper();

    public List<Apc.ApcMessage> createRandomProtobufApcMessages(int count) {
        List<Apc.ApcMessage> list = new ArrayList<>();
        long now = ZonedDateTime.now().toEpochSecond();
        for (int i=0; i < count; i++) {
            Apc.CountQuality countQuality = countQualities[ran.nextInt(COUNT_QUALITIES_LENGTH)];
            list.add(Apc.ApcMessage.newBuilder()
                    .setPassengerDoorCount(
                            Apc.PassengerDoorCount.newBuilder()
                                    .setRecordedAtTime(now)
                                    .setDoorId(1)
                                    .setCountQuality(countQuality)
                                    .addPassengerCounting(Apc.PassengerCount.newBuilder()
                                            .setPassengerClass(
                                                    Apc.PassengerCount.PassengerClass.ADULT)
                                            .setPassengersIn(ran.nextInt(50))
                                            .setPassengersOut(ran.nextInt(50))
                                            .build())
                                    .addPassengerCounting(Apc.PassengerCount.newBuilder()
                                            .setPassengerClass(
                                                    Apc.PassengerCount.PassengerClass.CHILD)
                                            .setPassengersIn(ran.nextInt(50))
                                            .setPassengersOut(ran.nextInt(50))
                                            .build())
                                    .build())
                    .build());
        }
        return list;
    }


    public List<ApcEvent> createRandomJsonApcMessages(int count, boolean validate) throws ProcessingException{
        List<ApcEvent> list = new ArrayList<>();

        final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        final JsonSchema schema = factory.getJsonSchema("resource:/schemas/apc.json");

        ObjectMapper objectMapper = new ObjectMapper();
        long now = ZonedDateTime.now().toEpochSecond();

        for (int i=0; i < count; i++) {
            list.add(ApcEvent.builder()
                    .vehicleId(vehicleIDs[ran.nextInt(VEHICLEIDS_LENGTH)])
                    .doorId("1")
                    .recordedAtTime(now)
                    .countQuality(jsonCountQualities[ran.nextInt(JSON_COUNT_QUALITIES_LENGTH)])
                    .passengerCount(ApcEvent.PassengerCount.builder()
                            .passengerClass(jsonPassengerClasses[ran.nextInt(JSON_PASSENGER_CLASSES_LENGTH)])
                            .passengersIn(ran.nextInt(50))
                            .passengersOut(ran.nextInt(50))
                            .build())
                    .build());

            if (validate) {

                ProcessingReport report = schema.validate(objectMapper.convertValue(list.get(i), JsonNode.class));

                if (!report.isSuccess()) {
                    System.out.println("Failed to validate!");
                    System.out.println(report.toString());
                }
            }


        }
        return list;
    }

 }
