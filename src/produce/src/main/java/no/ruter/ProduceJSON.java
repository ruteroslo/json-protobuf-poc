package no.ruter;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import no.ruter.models.ApcEvent;
import no.ruter.mqtt.MqttProducer;
import no.ruter.mqtt.MqttProperties;
import no.ruter.ota.Apc;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.List;

/**
 * Created by Bjørn Hjelle, Acando on 03.10.2017.
 */
public class ProduceJSON {

    private MqttProducer mqttProducer;
    private MqttProperties mqttProperties;

    private ObjectMapper objectMapper;

    private int count = 10;

    private EventFactory eventFactory = new EventFactory();

    public ProduceJSON() {
        objectMapper = new ObjectMapper();
        mqttProperties = new MqttProperties();
        mqttProperties.setClientId("protobuf-test");
        mqttProperties.setServerURI("tcp://localhost:1883");
        mqttProperties.setUsername("ruter");
        mqttProperties.setPassword("Ruter2016");
        mqttProducer = new MqttProducer(mqttProperties);
    }

    public void run() throws Exception {

        List<ApcEvent> jsonApcMessages =  eventFactory.createRandomJsonApcMessages(count, false);
        for (ApcEvent message : jsonApcMessages) {
            mqttProducer.produce(objectMapper.writeValueAsBytes(message), "consumeproducetest/apc/json", false);
        }
        mqttProducer.close();
    }

    public static void main(String[] args ) throws Exception{
       ProduceJSON producer = new ProduceJSON();
       producer.run();
    }

}
