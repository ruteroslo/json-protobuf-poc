# ITxPT Proof of Concept: JSON vs protobuf



## Findings
The findings of the PoC can be summarised as follows: 

* Protobuf is more efficient, but JSON is efficient enough for use in the ITxPT architecture.
* Protobuf is smaller on the wire, but in the ITxPT context we do not think JSON message size will be an issue
* Protobuf has been designed with forward/backward compatibility in mind, in a way that will work in all supported programming languages.
* A JSON format can also be designed to be forward/backward compatible, but this is done differently in different programming languages.
* Protobuf is more developer friendly, as the protobuf compiler will generate code for several programming languages.
* All major programming languages have good support for JSON, but each language supports it in its own way.

For details, see separate sections below.

Based on the findings we suggest that both JSON and Protocol Buffers are included as messaging formats in ITxPT.

## Background
The mission of the ITxPT Initiative (http://itxpt.org) is to support the deployment of standards and practices for onboard plug-and-play IT-systems for public transport and the relevant back-office features.  The ITxPT working group 6 has as its main objective to identify, analyse and implement relevant protocols in the scope of ITxPT.

At the IP-level the working group has reached a consensus that TCP should be used as the transfer protocol. Further more, MQTT (http://mqtt.org/) has been selected as a strong candidate for a messaging protocol both in the onboard architecture and over the air between the vehicles and the back offices.

For the data message format, both XML, JSON and Protocol Buffers have been proposed. XML is generally considered to be too verbose and to be more difficult to parse and process than JSON (ref https://www.w3schools.com/js/js_json_xml.asp).

JSON and protobuf have both pros and cons and working group 6 has identified the need to investigate whether one or the other should be chosen as the recommended data message format in the ITxTP architecture. It is also an option to include both formats in the architecture.

## PoC goals
The JSON vs protobuf Proof of Concept will look into the two technologies with respect to:

* efficiency
* schema support
* forward and backwards compatibility
* language and tool support

The PoC will be a mix of investigations and actual testing conducted with test programs, tools and scripts, with the aim to provide information and test results so that ITxPT working group 6 can address the issue of JSON vs protobuf in the next work group meeting (mid november 2017).

The PoC does not consider other formats than JSON and protobuf. For a comparison with other formats read this: http://labs.criteo.com/2017/05/serialization/

## Data message formats
For the purpose of this Poc we have defined JSON and protobuf versions of [GPS and APC messages](doc/message_formats.md).
Note that these are NOT the official formats defined by ITxPT.


## Efficiency
While JSON is a text-based format, protobuf is binary and therefore smaller on the wire and in general considered more efficient.
In the PoC we look into what this actually means for the [application of the formats in ITxTP architecture](doc/efficiency.md).

## Schema support
While Protobuf has the protocol buffer language to define the structure of the protocol buffer data, JSON was not designed with strict structure in mind. However, initiatives have been taken to add support for annotation and validation of JSON messages. We will therefore look into how protobuf and JSON compares [with respect to schema support](doc/json_schema.md).

## Forward and backward compatibility
Data formats will change over time, we therefore need to look into how consumers and producers of messages are affected when:

1. fields are renamed
2. fields are removed
3. fields are added
4. other changes (such as a change in a list of legal values)

With actual examples we will compare JSON and protobuf in regard to [forward and backward compatibility](doc/forw_backw_comp.md).

## Language and tool support
We will look into ease of use of the two data formats in [different programming languages](doc/language_and_tool_support.md) and how they are supported in development tools.
